<footer class="footer">
    
    <div class="footer__col">
        <p class="title strong c-white">Services</p>
        <nav>
            <ul>
                <li>
                    <a href="#">Catering Leeds</a>
                </li>
                <li>
                    <a href="#">City Signs</a>
                </li>
                <li>
                    <a href="#">Civic Flavour</a>
                </li>
                <li>
                    <a href="#">Cleaning</a>
                </li>
                <li>
                    <a href="#">Fleet services</a>
                </li>
                <li>
                    <a href="#">MOT</a>
                </li>
                <li>
                    <a href="#">Presto</a>
                </li>
            </ul>
        </nav>
    </div>

    <div class="footer__col">
        <p class="title strong c-white">Policy</p>
        <nav>
            <ul>
                <li>
                    <a href="#">Accessibility</a>
                </li>
                <li>
                    <a href="#">Privacy Statement</a>
                </li>
                <li>
                    <a href="#">Terms and Conditions</a>
                </li>
            </ul>
        </nav>
    </div>

    <div class="footer__col">
        <p class="title strong c-white">Contact us</p>
        <ul>
            <li>
                Telephone: <a href="tel:+44 000 000 000">xxx xxx xxx</a>
            </li>
            <li>
                Email: <a href="mailto:xxx@xxx.xxx" target="_blank">xxx@xxx.xxx</a>
            </li>
            <li>
                <a href="/callback">Request a callback</a>
            </li>
        </ul>
    </div>

    <div class="footer__col">
        <img src="test/dist/images/lcc-logo.svg" alt="Leeds city council" />
        <p class="small c-white">Copyright Leeds City Council <?php echo date('Y');?></p>
        <p class="small c-white">Official local authority website for Leeds providing information on local services.</p>
    </div>

</footer>