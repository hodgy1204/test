<header class="header">
    <div class="branding">
        <a href="/">
            <img src="test/dist/images/logo.svg" alt="Civic Enterprise Leeds" />
        </a>
    </div>
    <nav class="navbar__navigation" role="navigation">
        <ul class="menu navbar__menu">
            <li class="active"> <a href="/home">Home</a> </li>
            <li> <a href="/our-services">Our Services</a> </li>
            <li> <a href="/about-us">About us</a> </li>
            <li> <a href="/contact">Contact</a> </li>
        </ul>
    </nav>

    <div class="search search__wrapper">
        <form>
            <label class="search-label" for="search">
                <span>Search our website</span>
            </label>
            <input id="search" type="text" placeholder="Search our website">
            <input class="button" type="button" value="Go">
        </form>
    </div>
    <a href="#menu" role="button" class="navbar__toggle" aria-expanded="false" aria-controls="menu" aria-label="Toggle navigation">
        <i class="fa fa-bars"></i>
    </a>
    <div class="menu__mobile"></div>
</header>