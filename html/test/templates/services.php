<section class="services">

        <div class="service purple2">
            <a href="#">
                <h2>Catering Leeds</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <?php require('test/dist/images/arrow-circle-right-solid.svg'); ?>
            </a>
        </div>

        <div class="service purple">
            <a href="#">
                <h2>Business Support Centre</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <?php require('test/dist/images/arrow-circle-right-solid.svg'); ?>
            </a>
        </div>

        <div class="service blue">
            <a href="#">
                <h2>Services for schools</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <?php require('test/dist/images/arrow-circle-right-solid.svg'); ?>
            </a>
        </div>

        <div class="service teal2">
            <a href="#">
                <h2>Fleet services</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <?php require('test/dist/images/arrow-circle-right-solid.svg'); ?>
            </a>
        </div>

        <div class="service purple">
            <a href="#">
                <h2>Cleaning services</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <?php require('test/dist/images/arrow-circle-right-solid.svg'); ?>
            </a>
        </div>

        <div class="service blue">
            <a href="#">
                <h2>City signs</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <?php require('test/dist/images/arrow-circle-right-solid.svg'); ?>
            </a>
        </div>

        <div class="service teal2">
            <a href="#">
                <h2>Presto</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <?php require('test/dist/images/arrow-circle-right-solid.svg'); ?>
            </a>
        </div>

        <div class="service teal">
            <a href="#">
                <h2>Flavour</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <?php require('test/dist/images/arrow-circle-right-solid.svg'); ?>
            </a>
        </div>

    </section>