<!DOCTYPE html>
<!--[if IE 8 ]><html id="html" class="ie ie8 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]><html id="html" class="ie ie9 no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html id="html" class="no-js mb" lang="en"> <!--<![endif]-->
<head>
	<script>document.getElementById("html").className = document.getElementById("html").className.replace( /(?:^|\s)no-js(?!\S)/g , 'js' )</script>
	<!--[if IE 9]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<![endif]-->
	<title>Leeds civic enterprise</title>
    <meta charset="UTF-8" />
	<meta name="viewport" content="initial-scale=1.0,width=device-width" />
	<meta property="og:url"           content="http://test.test" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="Leeds civic enterprise" />


	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap" rel="stylesheet">
    <link rel='stylesheet' id='css-styles-css'  href='test/dist/styles/style.css' type='text/css' media='all' />

    <!--favicon-->
</head>
<body>

<div class="container">

    <?php require('test/templates/header.php'); ?>

    <?php require('test/templates/hero.php'); ?>

    <?php require('test/templates/services.php'); ?>

    <?php require('test/templates/intro.php'); ?>

    <?php require('test/templates/footer.php'); ?>

	<div class="overlay"></div>
</div>


</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="test/dist/scripts/scripts.min.js"></script>
<script src="https://kit.fontawesome.com/26796d67ca.js"></script>

</html>