$(document).ready(function () {

    // menu 
    function mobileNav() {

        let wW = $(window).width();
        if (wW > 980) {
            $('.header').removeClass('sticky');
            $('.container').removeClass('mobile');
            $('.navbar__navigation').detach().appendTo('.header');
            $('.search__wrapper').detach().appendTo('.header');
        } else {
            $('.header').addClass('sticky');
            $('.container').addClass('mobile');
            $('.navbar__navigation').detach().appendTo('.menu__mobile');
            $('.search__wrapper').detach().appendTo('.menu__mobile');
        }
    }
    mobileNav();

    $(window).resize(function () {
        mobileNav();
    });

    // Hamburger
    $('.navbar__toggle').on('touchend click', function (e) {
        e.preventDefault();

        $(this).attr('aria-expanded', function (i, attr) {
            return attr == 'true' ? 'false' : 'true'
        });

        if ($(this).attr('aria-expanded') === 'false') {
            $('.menu__mobile').removeClass('active');
            $('.overlay').removeClass('active');
        } else {
            $('.menu__mobile').addClass('active');
            $('.overlay').addClass('active');
        }

    });


}); //document ready

