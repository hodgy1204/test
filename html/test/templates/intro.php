<section class="intro">
    <div class="intro__wrapper">
        <div class="col">
            <?php require('test/dist/images/cooking-icon.svg');?>
        </div>
        <div class="col">
            <h2>Leeds City Council provides excellent services to the people of Leeds every day.</h2>
            <p>Leeds City Council is made up of many amazing frontline services which include; catering, cleaning, property maintenance, fleet management, legal services, business support centre to name just a few.</p>
            <p>Each day these teams deliver fantastic standards for the people of Leeds. Many of our council services are desirable to other companies so Civic Enterprise Leeds has been developed to allow key services to develop business opportunities, build relationships and start to trade.</p>
            <p>Civic Enterprise Leeds already hold contracts with the NHS, the West Yorkshire Ambulance Service, Ringways Ford to name a few. We also provide services for other local authorities in the UK.</p>
        </div>
    </div>
</section>