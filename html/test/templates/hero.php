<section class="hero">
    <div class="hero__meta">
        <h1>Excellence you can rely on</h1>
        <p class="large">Catering, clean, property maintenance, fleet management, legal services, business support and more...</p>
    </div>
    <div class="hero__image">
        <?php require('test/dist/images/cleaning-icon.svg'); ?>
    </div>
</section>